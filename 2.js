//clase Forma
var Forma=function(datos){
	this.area=null;

	this.getArea=function(){
		return this.area;
	}
	this.setArea=function(valor){
		this.area=valor || 0;
	}
	this.constructForma=function(datos){
		this.setArea(datos.area);
	}

	this.constructForma(datos);
}

var objeto=new Forma({area:100});



//clase Cuadrado:
var Cuadrado=function(valores){
	this.lado=null;

	this.getLado=function(){
		return this.lado;
	}
	this.setLado=function(valor){
		this.lado=valor || 0;
	}

	this.constructCuadrado=function(valores){
		this.setLado(valores.lado);
		Forma.call(this,valores);
	}

	this.constructCuadrado(valores);
}


var objeto1=new Cuadrado({lado:10, area:200});

console.log(objeto);
console.log(objeto1);