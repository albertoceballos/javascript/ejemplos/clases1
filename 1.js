
var Persona=function(padre){
	var nombre;
	var apellidos;
	var fechaNacimiento;
	var direccion;

	//getter para nombre
	this.getNombre=function(){
		return nombre.toUpperCase();
	};
	//setter para nombre
	this.setNombre=function(valor){
		nombre=valor || "";
	};

	//getter para apellidos
	this.getApellidos=function(){
		return apellidos.toUpperCase();
	};
	//setter para apellidos
	this.setApellidos=function(valor){
		apellidos=valor || "";
	};
	//getter para direccion
	this.getDireccion=function(){
		return direccion;
	};
	//setter para direccion
	this.setDireccion=function(valor){
		direccion=valor || "";
	};
	//getter para la fecha de nacimiento
	this.getFechaNacimiento=function(){
		return fechaNacimiento;
	};
	//setter para la fecha de nacimiento:
	this.setFechaNacimiento=function(valor){
		fechaNacimiento=valor || "1/1/1990";
	}

	//getter para devolver el nombre completo:
	this.getNombreCompleto=function(){
		var aux;
		aux=this.getNombre()+" "+this.getApellidos();
		return aux;
	}

	this.getEdad=function(){
		var hoy=new Date();
		var vector=this.getFechaNacimiento().split("/");
		var fn=new Date(vector[2],vector[1],vector[0]);
		return(hoy.getFullYear()-fn.getFullYear());
	}

	//función constructora
	this.persona=function(valores){
		this.setNombre(valores.nombre);
		this.setApellidos(valores.apellidos);
		this.setDireccion(valores.direccion);
		this.setFechaNacimiento(valores.fechaNacimiento);
	}
	this.persona(padre);
}

//Clase trabajador

var Trabajador=function(valores){
	//propiedades privadas:
	var sueldo;
	var hijos;
	var empresa;
	var fechaEntrada;

	//función getter de sueldo:
	this.getSueldo=function(){
		return sueldo;
	}
	//función setter  de sueldo:
	this.setSueldo=function(valor){
		sueldo=valor || 0;
	}
	//función getter de empresa:
	this.getEmpresa=function(){
		return empresa;
	}
	//función setter de empresa:
	this.setEmpresa=function(valor){
		empresa=valor || "";
	}
	//función getter de hijos:
	this.getHijos=function(){
		return hijos;
	}
	//función setter de empresa:
	this.setHijos=function(valor){
		hijos=valor || 0;
	}
	//función getter de fechaEntrada:
	this.getFechaEntrada=function(){
		return fechaEntrada;
	}
	//función setter de fechaEntrada:
	this.setFechaEntrada=function(valor){
		fechaEntrada=valor || "1/1/2000";
	}

	//función constructora:
	this.constructTrabajador=function(){
		this.persona();
		this.setSueldo(valores.sueldo);
		this.setEmpresa(valores.empresa);
		this.setHijos(valores.hijos);
		this.setFechaEntrada(valores.fechaEntrada);
	}
	//llamar función constructora:
	this.constructTrabajador();

}

	Trabajador.prototype = new Persona({});


//nuevo objeto "alberto" de clase Trabajador:
var alberto=new Trabajador({
	sueldo:1000,
	hijos:0,
	empresa:"INEM",
	fechaEntrada:"15/3/2018",
	nombre: "alberto";
});


//nuevo objeto "jose" de clase Persona
var jose=new Persona({
	nombre:"Jose",
	apellidos:"García Pérez",
	direccion:"",
	fechaNacimiento:"1/1/2000",
});

//console.log(jose.getNombreCompleto());
//console.log(jose.getFechaNacimiento());
//console.log(jose.getEdad());

console.log(alberto.getSueldo());
console.log(alberto.getHijos());
console.log(alberto.getEmpresa());
