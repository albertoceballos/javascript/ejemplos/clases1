//clase Persona:
var Persona=function(datos){
	//propiedades:
	this.nombre=null;
	this.direccion=null;
	this.correoElectrónico=null;

	//métodos getter y setter:
	this.getNombre=function(){
		return this.nombre;
	}
	this.setNombre=function(arg){
		this.nombre=arg || "";
	}
	this.getDireccion=function(){
		return this.direccion;
	}
	this.setDireccion=function(arg){
		this.direccion=arg || "";
	}
	this.getCorreoElectronico=function(){
		return this.correoElectrónico;
	}
	this.setCorreoElectronico=function(arg){
		this.correoElectrónico=arg || "";
	}

	//métodos propios:
	this.crear=function(){
		console.log('Creando persona');
	}
	this.borrar=function(){
		console.log('Borrando');
	}

	//constructor
	this.constructPersona=function(datos){
		this.setNombre(datos.nombre);
		this.setDireccion(datos.direccion);
		this.setCorreoElectronico(datos.correoElectrónico);
	}

	//llamar al constructor:
	this.constructPersona(datos);
}

//clase Cliente:
var Cliente=function(valores){
		//propiedades:
		this.numeroCliente=null;
		this.fechaAlta=null;

		//métodos getter y setter:

		this.getNumeroCliente=function(){
			return this.numeroCliente;
		}
		this.setNumeroCliente=function(arg){
			this.numeroCliente=arg || 0;
		}
		this.getFechaAlta=function(){
			return this.fechaAlta;
		}
		this.setFechaAlta=function(arg){
			this.fechaAlta=arg || "1/1/2000";
		}

		//método verFechaAlta
		this.verFechaAlta=function(){
			console.log(this.fechaAlta);
		}

		//constructor
		this.constructCliente=function(valores){
			this.setNumeroCliente(valores.numeroCliente);
			this.setFechaAlta(valores.fechaAlta);		
		}
		//llamar al constructor:
		this.constructCliente(valores);
		Persona.call(this,valores);
}

//Clase usuario
var Usuario=function(valores){
		//propiedades:
		this.codigoUsuario=null;

		//metodos getter y setter
		this.getCodigoUsuario=function(){
			return this.codigoUsuario;
		}
		this.setCodigoUsuario=function(arg){
			this.codigoUsuario=arg || 0;
		}
		//métodos propios:

		this.autorizar=function(){
			console.log('Autorizando');
		}
		this.crear=function(){
			console.log('Creando usuario');
		}

		//constructor:

		this.constructUsuario=function(valores){
			this.setCodigoUsuario(valores.codigoUsuario);
			//al heredar mediante Prototype hay que llamar a l constructor del padre
			this.constructPersona(valores);
		}
		this.constructUsuario(valores);
}

//Heredo de la clase padre Persona mediante Prototype porque tienen métodos llamadoss iguales que con call se sobreescribirían 
Usuario.prototype = new Persona({});



//Nuevo objeto persona1 de clase Persona
var persona1=new Persona({
		nombre:"Jose Gonzalez",
		direccion:"C/Gran vía 1",
		correoElectrónico:"aaad@dfdf.es",
});

var cliente1=new Cliente({
		numeroCliente:10,
		fechaAlta:"20/3/2011",
		nombre:"Ana Rodriguez",
		direccion:"Paseo del Alta 4",
});

var usuario1=new Usuario({
		codigoUsuario:"fg3445",
		nombre:"Alberto Pérez",
		direccion:"C/Elcano",
		correoElectrónico:"aaa@ppppp.es",
});

console.log(persona1);
console.log(cliente1);
console.log(cliente1.verFechaAlta());
console.log(usuario1);