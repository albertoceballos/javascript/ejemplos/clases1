//clase padre SeleccionFutbol

var SeleccionFutbol=function(valores){

	//propiedades públicas
	this.id=null;
	this.nombre=null;
	this.apellidos=null;
	this.edad=null;

	//métodos getter y setter de id:
	this.getId=function(){
		return this.id;
	} 
	this.setId=function(valor){
		this.id=valor || 0;
	}
	//métodos getter y setter de nombre:
	this.getNombre=function(){
		return this.nombre;
	}
	this.setNombre=function(valor){
		this.nombre=valor || "";
	}
	//métodos getter y setter de apellidos:
	this.getApellidos=function(){
		return this.apellidos;
	}
	this.setApellidos=function(valor){
		this.apellidos=valor || "";
	}
	//métodos getter y setter de eded:
	this.getEdad=function(){
		return this.edad;
	}
	this.setEdad=function(valor){
		this.edad=valor || "";
	}

	//método concentrarse:
	this.concentrarse=function(){
		console.log('concentrados!');
	}
	//métdo viajar
	this.viajar=function(){
		console.log('viajando!');
	}

	//Constructor:
	this.constructSeleccionFutbol=function(valores){
		this.setId(valores.id);
		this.setNombre(valores.nombre);
		this.setApellidos(valores.apellidos);
		this.setEdad(valores.edad);
	}

	//llamada al constructor:
	this.constructSeleccionFutbol(valores);

}


//clase Futbolista:
var Futbolista=function(datos){
	//propiedades:
	this.dorsal=null;
	this.demarcacion=null;

	//metodos getter y setter:
	this.getDorsal=function(){
		return this.dorsal;
	}
	this.setDorsal=function(valor){
		this.dorsal=valor || 0;
	}
	this.getDemarcacion=function(){
		return this.demarcacion;
	}
	this.setDemarcacion=function(valor){
		this.demarcacion=valor || "";
	}

	//método jugarPartido:
	this.jugarPartido=function(){
		console.log('Jugando partido!');
	}
	//método entrenar:
	this.entrenar=function(){
		console.log('Entrenando!');
	}

	//constructor:
	this.constructFutbolista=function(datos){
		this.setDorsal(datos.dorsal);
		this.setDemarcacion(datos.demarcacion);
	}
	//llamar constructor:
	this.constructFutbolista(datos);

	//heredar de la clase seleccionFutbol:
	SeleccionFutbol.call(this, datos);

}

//Clase Entrenador:
var Entrenador=function(datos){

	//propiedades:
	this.idFederacion=null;

	//metodos getter y setter:
	this.getIdFederacion=function(){
		return this.idFederacion;
	}
	this.setIdFederacion=function(valor){
		idFederacion=valor || 0;
	}
	//método dirigirPartido:
	this.dirigirPartido=function(){
		console.log('Dirigiendo partido');
	}
	//método dirigirEntrenamiento:
	this.dirigirEntrenamiento=function(){
		console.log('Dirigiendo entrenamiento');
	}
	//constructor:
	this.constructEntrenador=function(datos){
		this.setIdFederacion(datos.idFederacion);
	}
	//llamar constructor:
	this.constructEntrenador(datos);
	SeleccionFutbol.call(this, datos);

}

//Clase masajista:
var Masajista=function(datos){
	//propiedades:
	this.titulacion=null;
	this.aniosExperiencia=null;

	//métodos getter y setter:
	this.getTitulacion=function(){
		return titulacion;
	}
	this.setTitulacion=function(valor){
		titulacion=valor || "";
	}
	this.getAniosExperiencia=function(){
		return aniosExperiencia;
	}
	this.setAniosExperiencia=function(valor){
		aniosExperiencia=valor || 0;
	}
	//método darMasaje:
	this.darMasaje=function(){
		console.log('Dando masaje');
	}

	//constructor:
	this.constructMasajista=function(datos){
		this.setTitulacion(datos.titulacion);
		this.setAniosExperiencia(datos.aniosExperiencia);
	}

	//llamar constructor:
	this.constructMasajista(datos);

	//heredar de SeleccionFutbol:
	SeleccionFutbol.call(this,datos);
}



//crear objeto seleccion1 de clase SeleccionFutbol:
var seleccion1=new SeleccionFutbol({
	id:2,
	nombre:"",
	apellidos:"",
	edad:34,
});

//crear objetos de clase Futbolista:
var futbolista1=new Futbolista({
	dorsal:14,
	demarcacion:"Centrocampista",
	nombre:"Sergi",
	apellidos:"Busquets",
	edad:31,
});

var futbolista2=new Futbolista({
	dorsal:9,
	demarcacion:"Delantero",
	nombre:"Rodrigo",
	apellidos:"",
	edad:25,
});



//crear objeto entrenador1 de clase Entrenador:

var entrenador1=new Entrenador({
	idFederacion:76,	
	nombre:"Luis Enrique",
	apellidos:"Martínez",
	edad:48,
});

//crear objeto masajista1 de clase Masajista:

var masajista1=new Masajista({
	titulacion:"Técnico Superior",
	aniosExperiencia:6,
	nombre:"Manuel",
	apellidos:"López Benítez",
	edad:40,
});


console.log(seleccion1);
console.log(futbolista1);
console.log(futbolista2);
console.log(entrenador1);
console.log(masajista1);



